# Recource monitoring
## Grafana, Telegraf, InfluxDB, Nuxt, Nest, MongoDB, ElasticSearch, Artillery


## Run

- ```git clone --recurse-submodules --remote-submodules https://gitlab.com/resource-monitoring/infrastructure.git```
- ```cd resource-monitoring```
- ```docker-compose -f "docker-compose.yml" up -d --build ```
- Open [Grafana](http://localhost:3000/d/000000127/telegraf-system-dashboard?orgId=1&refresh=1m), login: admin, password: admin
- Run Artillery Load testing:
  ```
  docker run --network=resource-monitoring_default --rm -it \ 
        -v "$(pwd)/artillery/":/scripts \
        artilleryio/artillery:latest \
        run /scripts/load-test.yml
  ```

## Screenshots
![Load testing](https://i2.paste.pics/ea80aa680b8976ad3ebeec37ec413ba9.png "Load testing")
![Grafana](https://i2.paste.pics/f05057f236b1f6e122b975a1ef9976eb.png "Grafana")

